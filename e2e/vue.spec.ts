import { test, expect } from '@playwright/test';

// See here how to get started:
// https://playwright.dev/docs/intro
test.describe('Log in', () => {
  test.beforeEach(async ({page}) => {
    await page.goto('/')
  })

  test('Visits the app root url', async ({ page }) => {
    await expect(page).toHaveTitle('Vite App')
  })

  test('User can log in', async ({page}) => {
    await page.getByLabel('Din mail').fill('test@test.com')

    await page.getByLabel('Ditt lösenord').fill('Test123')
    
    await page.getByRole('button', {name: 'Skicka'}).click()

    await expect(page.getByRole('button', {name: 'Logga ut'})).toBeVisible()
  })

  test('Show error for invalid user', async ({page}) => {
    await page.getByLabel('Din mail').fill('test@test.com')

    await page.getByRole('button', {name: 'Skicka'}).click()

    await expect(page.getByText('Felaktigt användarnamn eller lösenord')).toBeVisible()
  })
})